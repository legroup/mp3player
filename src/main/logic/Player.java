package main.logic;

import de.hsrm.mi.eibo.simpleplayer.SimpleAudioPlayer;
import de.hsrm.mi.eibo.simpleplayer.SimpleMinim;
import main.models.SongBean;

import java.io.FileNotFoundException;
import java.util.Observable;

/**
 *
 */
public class Player extends Observable {
    /**
     *
     */
    private SimpleMinim minim;
    /**
     *
     */
    public SimpleAudioPlayer audioPlayer;
    /**
     *
     */
    private SongBean songPlaying;

    /**
     * play song from file directly
     * @param filename
     */
    public void play(String filename) throws FileNotFoundException {
        this.minim = new SimpleMinim(true);
        if (audioPlayer != null){
            audioPlayer.pause();
        }
        audioPlayer = this.minim.loadMP3File(filename);
        play();
    }

    /**
     *  play loaded song
     */
    public void play() throws FileNotFoundException{
        if (audioPlayer != null){
            audioPlayer.play();
        }else {
            throw new FileNotFoundException("No Song loaded");
        }
    }

    /**
     * play song from SongBean
     *
     * @param song
     */
    public void playSong(SongBean song) {
        this.songPlaying = song;
        try{
            play(song.getFile().getPath());
            setChanged();
            notifyObservers(song);
        }catch (FileNotFoundException e){
            System.out.println("File not found");
        }
    }

    /**
     * pause currently playling song
     *
     */
    public void pause() {
        audioPlayer.pause();
    }

    /**
     * stop current song
     */
    public void stop() {
        if(audioPlayer != null) {
            audioPlayer.pause();
            audioPlayer = null;
            setChanged();
            notifyObservers(null);
        }
    }

    /**
     * sets volume (using gain)
     * @param value
     */
    public void volume(float value) {

        if(audioPlayer != null){
            audioPlayer.setGain(value);
        }
    }

    /**
     * set balance
     *
     * @param value
     */
    public void balance(float value) {
        audioPlayer.setBalance(value);
    }

    /**
     * get currently playing song
     *
     * @return
     */
    public SongBean getSongPlaying() {
        return songPlaying;
    }

    /**
     * exit routine to dispose minim thread
     */
    public void exit(){
        if (this.minim != null){
            this.minim.stop();
            this.minim.dispose();
        }
    }
}
