package main.logic;

import javafx.scene.control.Alert;
import javafx.stage.FileChooser;
import main.models.SongBean;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;

/**
 *
 */
public class Playlist extends Observable implements Serializable {
    /**
     *
     */
    public ArrayList<SongBean> songs = new ArrayList<>();
    /**
     *
     */
    private String name;

    /**
     *
     */
    private SongBean songPlaying;

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * indicates if a playlist is active
     */
    private Boolean active;

    /**
     *
     * @param active
     */
    Playlist(Boolean active){
        this.active = active;
    }

    /**
     * add song from file
     *
     * @param file
     */
    public void addFromFile(File file){
        try{
            this.add(new SongBean(file));
        }catch (FileNotFoundException e){
            new Alert(Alert.AlertType.ERROR,"Die Datei wurde nicht gefunden");
        }
    }

    /**
     * add song from SongBean
     * @param song SongBean
     */
    public void add(SongBean song){
        this.songs.add(song);
        setChanged();
        notifyObservers(this);
    }

    /**
     *
     * @return
     */
    public Boolean getActive() {
        return active;
    }

    /**
     *
     * @param active
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     *
     * get last song in playlist
     * @return
     */
    public SongBean getLastSong(){
        this.songPlaying = this.songs.get(this.songs.size() - 1);
        return this.songPlaying;
    }

    /**
     * get currently playing song
     * @return
     */
    public SongBean getSongPlaying() {
        return songPlaying;
    }

    /**
     * set currently playing song and notify observers
     *
     * @param songPlaying
     */
    public void setSongPlaying(SongBean songPlaying) {
        this.songPlaying = songPlaying;
        setChanged();
        notifyObservers(this);
    }

    /**
     * set song playing by index
     * @param index
     * @return
     */
    public SongBean setSongPlaying(int index) {
        SongBean song = this.songs.get(index);
        setSongPlaying(song);
        return song;
    }
}
