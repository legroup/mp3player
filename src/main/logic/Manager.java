package main.logic;

import main.models.SongBean;
import java.io.*;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Random;

/**
 * Manager (Singelton)
 */
public class Manager extends Observable {
    /**
     * Manager instance
     */
    private final static Manager instance = new Manager();
    /**
     * Player instance
     */
    public final static Player player = new Player();
    /**
     * ArrayList of Playlists for a more advanced playlist management in the future
     */
    private ArrayList<Playlist> playlists = new ArrayList<Playlist>();
    /**
     * index of current playlist
     */
    private Integer currentPlaylist = 0;
    /**
     * shuffle current playlist
     */
    private boolean shuffle = false;

    /**
     * @return
     */
    public static Manager getInstance() {
        return instance;
    }

    /**
     *
     */
    public Manager() {
        this.playlists.add(new Playlist(true));
    }

    /**
     * pauses music
     */
    public void pause() {
        this.player.pause();
    }

    /**
     * play song from SongBean
     *
     * @param song
     */
    public void play(SongBean song) {
        this.player.playSong(song);
    }

    /**
     * play song on playlist and update playlist accordingly
     *
     * @param song
     */
    public void playFromPlaylist(SongBean song) {
        this.getCurrentPlaylist().setSongPlaying(song);
        this.player.playSong(song);
    }

    /**
     * resumes playing an already loaded song
     *
     * @throws FileNotFoundException
     */
    public void resume() throws FileNotFoundException {
        this.player.play();
    }

    /**
     * stops currently playing song
     *
     */
    public void stop() {
        this.player.stop();
    }

    /**
     * plays last added song
     *
     */
    public void playLastAdded() {
        this.player.playSong(this.getCurrentPlaylist().getLastSong());
    }

    /**
     * play from file
     *
     * @param file
     */
    public void playFile(File file) {
        playlists.get(currentPlaylist).addFromFile(file);
        playLastAdded();
    }

    /**
     * restarts current loaded song
     *
     */
    public void restartSong(){
        this.play(this.getCurrentPlaylist().getSongPlaying());
    }

    /**
     * add a file to the current playlist
     *
     * @param file
     */
    public void addFile(File file) {
        playlists.get(currentPlaylist).addFromFile(file);
    }

    /**
     * exist application
     */
    public void exit() {
        this.player.exit();
        System.exit(0);
    }

    /**
     * returns currently active playlist
     *
     * @return
     */
    public Playlist getCurrentPlaylist() {
        return playlists.get(currentPlaylist);
    }

    /**
     * sets currentPlaylist to a playlist index
     *
     */
    private void setPlaylist(Playlist playlist){
        this.currentPlaylist = this.playlists.indexOf(playlist);
    }

    /**
     * toggles shuffle
     */
    public void toggleShuffle() {
        if (!shuffle) {
            shuffle = true;
        } else {
            shuffle = false;
        }
    }

    /**
     * skips to next song
     */
    public void skipSong() {
        try {
            SongBean curr = player.getSongPlaying();
            int currIndex = getCurrentPlaylist().songs.indexOf(curr);
            if (currIndex == getCurrentPlaylist().songs.size() - 1){
                player.stop();
                return;
            }
            player.pause();
            SongBean song;
            if(shuffle){
                song = getCurrentPlaylist().setSongPlaying(nextShuffledSong());
            }else{
                song = getCurrentPlaylist().setSongPlaying(currIndex + 1);
            }
            player.playSong(song);
        } catch (IndexOutOfBoundsException e) {
            // Nothing to do if last song
        }
    }

    /**
     * goes back to last song
     */
    public void skipBackward() {
        try {
            SongBean curr = player.getSongPlaying();
            int currIndex = getCurrentPlaylist().songs.indexOf(curr);
            if (currIndex == 0){
                player.stop();
                return;
            }
            player.pause();
            SongBean song;
            if(shuffle){
                song = getCurrentPlaylist().setSongPlaying(nextShuffledSong());
            }else{
                song = getCurrentPlaylist().setSongPlaying(currIndex - 1);
            }
            player.playSong(song);
        } catch (IndexOutOfBoundsException e) {
            //Nothing to do if first song
        }
    }

    /**
     * goes to a random song on the current playlist
     * @return
     */
    public int nextShuffledSong(){
        SongBean curr= this.player.getSongPlaying();
        Playlist currPlaylist = getCurrentPlaylist();
        int rndIndex;
        do{
            rndIndex=new Random().nextInt(currPlaylist.songs.size()-1);
        }while(rndIndex==currPlaylist.songs.indexOf(curr));
        return rndIndex;
    }

    /**
     * load playlist from Playlist object and replace index 0 with it
     *
     * @param playlist
     */
    public void loadPlaylist(Playlist playlist){
        this.stop();
        this.playlists.add(0,playlist);
        setChanged();
        notifyObservers(this);
    }
}
