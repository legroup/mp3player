package main.models;

import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;
import ddf.minim.AudioPlayer;
import ddf.minim.Minim;
import de.hsrm.mi.eibo.simpleplayer.SimpleMinim;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.WritableImage;

import javax.imageio.ImageIO;
import java.io.*;
import java.util.concurrent.TimeUnit;

/**
 *
 */
public class SongBean implements Serializable {

    /**
     *
     */
    private File file;

    /**
     * holds meta data, gets restored by @prepareMetaData() after deserialization
     */
    private transient Mp3File meta;

    /**
     *
     */
    private String name;

    /**
     *
     */
    private String artist;

    /**
     *
     */
    private String durationString;

    /**
     * hold the coverimage, gets restored by @prepareCoverImage() after deserialization
     */
    private transient WritableImage coverImage;

    /**
     *
     * @param file
     * @throws FileNotFoundException
     */
    public SongBean(File file) throws FileNotFoundException{
        this.file = file;
        prepareMetaData();
        prepareCoverImage();
    }

    /**
     *
     */
    private void prepareMetaData(){
        try{
            meta = new Mp3File(file.getPath());
        }catch(UnsupportedTagException e){
            System.out.println("This file has unsupported Tags");
        }catch (IOException e){
            System.out.println("File not found");
        }catch (InvalidDataException e){
            System.out.println("File unreadable");
        }
        this.durationString = millisToString((int) this.meta.getLengthInMilliseconds());
        if (meta.hasId3v1Tag()){
            this.name = this.meta.getId3v1Tag().getTitle();
            this.artist = this.meta.getId3v1Tag().getArtist();
        }else{
            Minim minim = new SimpleMinim(true);
            AudioPlayer groove = minim.loadFile(file.getPath());

            this.name = groove.getMetaData().title();
            this.artist = groove.getMetaData().author();
        }
    }

    /**
     *
     */
    private void prepareCoverImage(){
        if (meta.hasId3v2Tag()) {
            byte[] imageData = meta.getId3v2Tag().getAlbumImage();
            if (imageData != null) {
                String mimeType = meta.getId3v2Tag().getAlbumImageMimeType();

                // Write image to file - can determine appropriate file extension from the mime type
                try{
                    RandomAccessFile coverImageFile = new RandomAccessFile("album-artwork", "rw");

                    coverImageFile.write(imageData);
                    coverImageFile.close();

                    coverImage = SwingFXUtils.toFXImage(ImageIO.read(new ByteArrayInputStream(imageData)),coverImage);

                }catch (FileNotFoundException ex) {
                    System.out.println("Problems caching cover image");

                }catch (IOException e){
                    System.out.println("Problems reading byte array into BufferedImage object");
                }

            }
        }
    }

    /**
     *
     * @return
     */
    public File getFile() {
        return file;
    }

    /**
     *
     * @param file
     */
    public void setFile(File file) {
        this.file = file;
    }

    /**
     *
     * @return
     */
    public Mp3File getMeta(){
        return meta;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public String getDurationString() {
        return durationString;
    }

    /**
     *
     * @param durationString
     */
    public void setDurationString(String durationString) {
        this.durationString = durationString;
    }

    /**
     * converts milliseconds to a string in the following format (mm:ss)
     *
     * @param millis
     * @return
     */
    private String millisToString(Integer millis){
        return String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(millis),
                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
    }

    /**
     *
     * @return
     */
    public String getArtist() {
        return artist;
    }

    /**
     *
     * @param artist
     */
    public void setArtist(String artist) {
        this.artist = artist;
    }

    /**
     *
     * @return
     */
    public WritableImage getCoverImage() {
        return coverImage;
    }

    /**
     *
     * @param coverImage
     */
    public void setCoverImage(WritableImage coverImage) {
        this.coverImage = coverImage;
    }

    /**
     * refill transient variables
     *
     * @param in
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void readObject(java.io.ObjectInputStream in)
            throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        prepareMetaData();
        prepareCoverImage();
    }
}
