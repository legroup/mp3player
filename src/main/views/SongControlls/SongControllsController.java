package main.views.SongControlls;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import main.models.SongBean;
import main.views.Controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Observable;
import java.util.Observer;
import java.util.ResourceBundle;

/**
 *
 */
public class SongControllsController extends Controller implements Observer {
    /**
     *
     */
    private boolean playState = false;

    /**
     *
     */
    private boolean localShuffle = false;

    /**
     *
     */
    @FXML
    private HBox songControlls;

    /**
     *
     */
    private Boolean playlistShowing = false;

    /**
     *
     */
    @FXML
    private FontAwesomeIconView playicon;

    /**
     *
     */
    @FXML
    private Button backButton;

    /**
     *
     */
    @FXML
    private Button skipForwardButton;

    /**
     *
     */
    @FXML
    private Button shuffleButton;

    /**
     *
     */
    @FXML
    private FontAwesomeIconView shuffleIcon;

    /**
     *
     */
    public void quit() {
        System.exit(0);
    }

    /**
     *
     */
    public void changeState() {
        try {
            if (!playState) {
                manager.resume();
                playicon.setGlyphName("PAUSE");
                playState = true;
            } else {
                manager.pause();
                playState = false;
                playicon.setGlyphName("PLAY");
            }
        } catch (FileNotFoundException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("File not found");

            alert.show();
        }
    }

    /**
     *
     * @throws IOException
     */
    public void onBtnTogglPlaylistClick() throws IOException {
        Node n = songControlls.getParent().getParent();
        StackPane ap = (StackPane) n.lookup("#switchAnchor");
        if (!playlistShowing) {
            URL url = getClass().getResource("../Playlist/playlist.fxml");
            TableView root = FXMLLoader.load(url);
            ap.getChildren().add(root);
            root.prefWidthProperty().bind(ap.widthProperty());
            root.prefHeightProperty().bind(ap.heightProperty());
            playlistShowing = true;
        } else {
            ap.getChildren().remove(1);
            playlistShowing = false;
        }
    }

    /**
     *
     * @param image
     * @return
     */
    private Background prepareBackgroundImage(Image image){
        if (image != null){
            BackgroundImage bg = new BackgroundImage(image,
                    BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
                    BackgroundSize.DEFAULT);
            return new Background(bg);
        }
        return null;
    }

    /**
     *
     * @param o
     * @param arg
     */
    @Override
    public void update(Observable o, Object arg) {
        SongBean song = (SongBean) arg;

        if (song!= null) {
            playState = true;
            playicon.setGlyphName("PAUSE");
        }else{
            playState = false;
            playicon.setGlyphName("PLAY");
        }
    }

    /**
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        super.initialize(url, rb);
        this.manager.player.addObserver(this::update);
    }

    /**
     *
     */
    public void stop() {
        try {
            manager.stop();
            playState = false;
            playicon.setGlyphName("PLAY");
        } catch (NullPointerException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("File not Found");
            alert.setTitle("Error");
            alert.show();
        }
    }

    /**
     *
     */
    public void skipBack() {
        backButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if ( event.getButton()==MouseButton.PRIMARY && event.getClickCount() == 2) {
                    manager.skipBackward();
                }else {
                    manager.restartSong();
                }
            }
        });
    }

    /**
     *
     */
    public void skipForward() {
        manager.skipSong();
    }

    /**
     *
     */
    public void shuffle() {
        manager.toggleShuffle();
        if (!localShuffle) {
            localShuffle = true;
            shuffleIcon.setFill(Color.valueOf("Blue"));

        } else {
            localShuffle = false;
            shuffleIcon.setFill(Color.valueOf("Black"));
        }

    }
}
