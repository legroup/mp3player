package main.views.SongTitle;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import main.models.SongBean;
import main.views.Controller;

import java.net.URL;
import java.util.Observable;
import java.util.Observer;
import java.util.ResourceBundle;

/**
 *
 */
public class SongTitleController extends Controller implements Observer {

    /**
     *
     */
    @FXML
    private Label artistLabel;

    /**
     *
     */
    @FXML
    private Label titleLabel;

    /**
     *
     * @param o
     * @param arg
     */
    @Override
    public void update(Observable o, Object arg) {
        SongBean song = (SongBean) arg;
        if (song == null){
            this.artistLabel.setText("Artist");
            this.titleLabel.setText("Title");
            return;
        }
        this.artistLabel.setText(song.getArtist());
        this.titleLabel.setText(song.getName());
    }

    /**
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        super.initialize(url, rb);
        this.manager.player.addObserver(this::update);
    }
}
