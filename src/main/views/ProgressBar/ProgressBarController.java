package main.views.ProgressBar;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Slider;
import javafx.scene.input.DragEvent;
import javafx.scene.layout.HBox;
import javafx.util.Duration;
import main.models.SongBean;
import main.views.Controller;

import java.net.URL;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *
 */
public class ProgressBarController extends Controller implements Observer {

    /**
     *
     */
    @FXML
    private HBox root;

    /**
     *
     */
    @FXML
    private ProgressBar progressBar;

    /**
     *
     */
    @FXML
    private Slider progressSlider;

    /**
     *
     */
    @FXML
    private Label currentTime;

    /**
     *
     */
    @FXML
    private Label length;



    Timeline updateCurrentTime = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {

        @Override
        public void handle(ActionEvent event) {
            if(manager.player.audioPlayer == null){
                updateCurrentTime.stop();
                return;
            }
            setCurrentProgress();
            updateCurrentTime.setOnFinished(this);{
                if(!manager.player.audioPlayer.isPlaying()){
                    try{
                        Thread.sleep(1000);
                    }catch(InterruptedException e){

                    }
                    manager.skipSong();
                }

            }

        }
    }));

    /**
     *
     * @param o
     * @param arg
     */
    @Override
    public void update(Observable o, Object arg) {
        SongBean song = (SongBean) arg;
        if(song != null){
            this.length.setText("" + millisToString(this.manager.player.audioPlayer.length()));
            updateCurrentTime.setCycleCount(Timeline.INDEFINITE);
            updateCurrentTime.play();
            this.progressSlider.setOnMouseReleased((ov)->{
                this.manager.player.audioPlayer.skip(progressToMillis((double) this.progressSlider.getValue(),this.manager.player.audioPlayer.length())
                        - this.manager.player.audioPlayer.position());
            });
        }else{
           reset();
        }
    }

    /**
     *
     */
    private void reset(){
        this.length.setText("-:--");
        this.currentTime.setText("-:--");
        this.progressSlider.setValue(0);
        this.progressBar.setProgress(0);
    }

    /**
     *
     */
    private void setCurrentProgress(){
        Integer millis = this.manager.player.audioPlayer.position();
        this.currentTime.setText("" + millisToString(millis));
        this.progressBar.setProgress(currentProgressFromMillis(millis, this.manager.player.audioPlayer.length()));
        this.progressSlider.setValue(currentProgressFromMillis(millis, this.manager.player.audioPlayer.length()));
    }

    /**
     *
     * @param current
     * @param total
     * @return
     */
    private double currentProgressFromMillis(Integer current, Integer total){

        return (double) ((current * 100.0f) / total) / 100;
    }

    /**
     *
     * @param progress
     * @param total
     * @return
     */
    private int progressToMillis(Double progress, Integer total){
        return  (int) (progress *  total);
    }

    /**
     *
     * @param millis
     * @return
     */
    private String millisToString(Integer millis){
        return String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(millis),
                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
    }

    /**
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        super.initialize(url, rb);
        this.progressBar.prefWidthProperty().bind(root.widthProperty().subtract(110));
        this.progressSlider.prefWidthProperty().bind(root.widthProperty().subtract(110));
        this.manager.player.addObserver(this::update);
    }
}
