package main.views.base;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import main.logic.Manager;

/**
 *
 */
public class Base extends Application {
    /**
     *
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("main/views/base/base.fxml"));
        primaryStage.setTitle("AJMM Player");
        primaryStage.setScene(new Scene(root, 300, 400));
        primaryStage.setMinWidth(300);
        primaryStage.setMinHeight(400);
        primaryStage.show();

    }

    /**
     *
     * @throws Exception
     */
    @Override
    public void stop() throws Exception {
        super.stop();
        Manager.getInstance().exit();
    }
}
