package main.views.base;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import main.views.Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 *
 */
public class BaseController extends Controller{
    /**
     *
     */
    @FXML
    private BorderPane base;

    /**
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb){
        super.initialize(url, rb);

        try{
            StackPane ap =  (StackPane) base.lookup("#switchAnchor");
            URL fileUrl = getClass().getResource("../SongInfo/songInfo.fxml");
            VBox songInfo = FXMLLoader.load(fileUrl);
            ap.getChildren().add(songInfo);
            songInfo.prefWidthProperty().bind(ap.widthProperty());
            songInfo.prefHeightProperty().bind(ap.heightProperty());
        }catch (IOException e){
            System.out.println(e);
        }
    }
}
