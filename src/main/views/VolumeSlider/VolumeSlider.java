package main.views.VolumeSlider;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.input.DragEvent;
import main.views.Controller;

import java.net.URL;
import java.util.Observable;
import java.util.Observer;
import java.util.ResourceBundle;

/**
 *
 */
public class VolumeSlider extends Controller {

    /**
     *
     */
    @FXML
    private Slider volumeSlider;

    /**
     *
     */
    @FXML
    private FontAwesomeIconView volumeIcon;

    /**
     *
     * @param url
     * @param rb
     */
    @FXML
    public void initialize(URL url, ResourceBundle rb) {
        super.initialize(url, rb);
        volumeSlider.valueChangingProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                manager.player.volume((float) volumeSlider.getValue());
                if (((int) volumeSlider.getValue() + 100) / 2 == 0) {
                    volumeIcon.setGlyphName("VOLUME_OFF");
                } else if (((int) volumeSlider.getValue() + 100) / 2 <= 50) {
                    volumeIcon.setGlyphName("VOLUME_DOWN");
                } else {
                    volumeIcon.setGlyphName("VOLUME_UP");
                }
            }
        });
    }
}
