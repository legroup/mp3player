package main.views;

import javafx.fxml.Initializable;
import main.logic.Manager;

import java.net.URL;
import java.util.ResourceBundle;

/**
 *
 */
public class Controller implements Initializable {
    /**
     *
     */
    public Manager manager;

    /**
     *
     * @param manager
     */
    public void setManager(Manager manager) {
        this.manager = manager;
    }

    /**
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.setManager(Manager.getInstance());
    }
}
