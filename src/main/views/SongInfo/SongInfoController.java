package main.views.SongInfo;


import javafx.fxml.FXML;

import javafx.scene.image.Image;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.*;

import main.models.SongBean;
import main.views.Controller;

import java.io.File;
import java.net.URL;
import java.util.Observable;
import java.util.Observer;
import java.util.ResourceBundle;

import static javafx.scene.layout.BorderWidths.AUTO;

/**
 *
 */
public class SongInfoController extends Controller implements Observer{

    /**
     *
     */
    @FXML
    private VBox songinfobox;

    /**
     *
     * @param event
     */
    public void handleDragOver(DragEvent event){
        Dragboard db = event.getDragboard();
        if (db.hasFiles()) {
            event.acceptTransferModes(TransferMode.COPY);
        } else {
            event.consume();
        }
    }

    /**
     *
     * @param event
     */
    public void handleDragDropped(DragEvent event) {
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasFiles()) {
            success = true;
            for (File file:db.getFiles()) {
                manager.playFile(file);

            }
        }
        event.setDropCompleted(success);
        event.consume();
    }

    /**
     *
     * @param o
     * @param arg
     */
    @Override
    public void update(Observable o, Object arg) {
        SongBean song = (SongBean) arg;
        if (song == null){
            this.loadBackgroundImage(null);
            return;
        }
        Image image = song.getCoverImage();
        this.loadBackgroundImage(image);
    }

    /**
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        super.initialize(url, rb);
        this.manager.player.addObserver(this::update);
        SongBean song = this.manager.player.getSongPlaying();
        if (song != null){
            this.loadBackgroundImage(song.getCoverImage());
        }

    }

    /**
     *
     * @param image
     * @return
     */
    private Background prepareBackgroundImage(Image image){
        if (image != null){
            BackgroundImage bg = new BackgroundImage(image,
                    BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
                    new BackgroundSize(AUTO,BackgroundSize.AUTO,true,true,true,false));
            return new Background(bg);
        }
        return null;
    }

    /**
     *
     * @param image
     */
    private void loadBackgroundImage(Image image){
        songinfobox.setBackground(prepareBackgroundImage(image));
    }
}
