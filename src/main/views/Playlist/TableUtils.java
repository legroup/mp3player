package main.views.Playlist;

import com.sun.javafx.scene.control.skin.TableViewSkinBase;

import javafx.event.EventHandler;
import javafx.geometry.Side;
import javafx.scene.Node;

import javafx.scene.control.ContextMenu;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.Label;

import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;

import com.sun.javafx.scene.control.skin.TableHeaderRow;

import javafx.stage.FileChooser;
import main.logic.Manager;
import main.logic.Playlist;

import java.io.*;

/**
 *
 */
public class TableUtils {

    /**
     * Make table menu button visible and replace the context menu with a custom context menu via reflection.
     * The preferred height is modified so that an empty header row remains visible. This is needed in case you remove all columns, so that the menu button won't disappear with the row header.
     * IMPORTANT: Modification is only possible AFTER the table has been made visible, otherwise you'd get a NullPointerException
     * @param tableView
     */
    public static void addCustomTableMenu( TableView tableView, Manager manager) {

        // enable table menu
        tableView.setTableMenuButtonVisible(true);

        // replace internal mouse listener with custom listener
        setCustomContextMenu( tableView, manager);

    }

    /**
     *
     * @param table
     * @param manager
     */
    private static void setCustomContextMenu( TableView table, Manager manager) {

        table.skinProperty().addListener((a, b, newSkin) -> {
            TableHeaderRow header = ((TableViewSkinBase) newSkin).getTableHeaderRow();

            for( Node child: header.getChildren()) {

                // child identified as cornerRegion in TableHeaderRow.java
                if( child.getStyleClass().contains( "show-hide-columns-button")) {

                    // get the context menu
                    ContextMenu columnPopupMenu = createContextMenu( table, manager);

                    // replace mouse listener
                    child.setOnMousePressed(me -> {
                        // show a popupMenu which lists all columns
                        columnPopupMenu.show(child, Side.BOTTOM, 0, 0);
                        me.consume();
                    });
                }
            }
        });
    }

    /**
     *
     * @param table
     * @return
     */
    private static ContextMenu createContextMenu(TableView table, Manager manager) {

        ContextMenu cm = new ContextMenu();

        // create new context menu
        CustomMenuItem cmi;

        // select all item
        Label showAll = new Label("Save Playlist");
        showAll.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Save Playlist");
                File file = fileChooser.showSaveDialog(table.getScene().getWindow());
                if (file != null) {
                    try {
                        FileOutputStream fos = new FileOutputStream(file + ".ajmm");
                        ObjectOutputStream oos = new ObjectOutputStream(fos);
                        oos.writeObject(manager.getCurrentPlaylist());
                        oos.close();
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            }

        });

        cmi = new CustomMenuItem(showAll);
        cmi.setHideOnClick(false);
        cm.getItems().add(cmi);

        // deselect all item
        Label hideAll = new Label("Load Playlist");
        hideAll.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Load Playlist");
                File file = fileChooser.showOpenDialog(table.getScene().getWindow());
                if (file != null) {
                    try {
                        FileInputStream fos = new FileInputStream(file);
                        ObjectInputStream ois = new ObjectInputStream(fos);
                        manager.loadPlaylist((Playlist) ois.readObject());
                        ois.close();

                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    } catch (ClassNotFoundException ex){
                        System.out.println(ex.getMessage());
                    }
                }
            }

        });

        cmi = new CustomMenuItem(hideAll);
        cmi.setHideOnClick(false);
        cm.getItems().add(cmi);

        return cm;
    }
}