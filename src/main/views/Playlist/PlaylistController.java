package main.views.Playlist;


        import javafx.collections.FXCollections;

        import javafx.fxml.FXML;

        import javafx.scene.control.TableRow;
        import javafx.scene.control.TableView;
        import javafx.scene.input.*;

        import main.logic.Playlist;
        import main.models.SongBean;
        import main.views.Controller;

        import java.io.File;
        import java.net.URL;
        import java.util.*;

/**
 *
 */
public class PlaylistController extends Controller implements Observer {
    /**
     *
     */
    @FXML
    private TableView root;

    /**
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        super.initialize(url, rb);
        this.manager.getCurrentPlaylist().addObserver(this::update);
        this.manager.addObserver(this::handleManagerUpdate);

        root.setItems(FXCollections.observableList(manager.getCurrentPlaylist().songs));

        TableUtils.addCustomTableMenu(root, manager);

        root.setRowFactory(tv -> {
            TableRow<SongBean> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (! row.isEmpty() && event.getButton()==MouseButton.PRIMARY
                        && event.getClickCount() == 2) {

                    SongBean song = row.getItem();
                    System.out.println(song.toString());
                    this.manager.playFromPlaylist(song);
                }
            });
            return row ;
        });


    }

    /**
     *
     * @param o
     * @param arg
     */
    public void handleManagerUpdate(Observable o, Object arg) {
        root.setItems(FXCollections.observableList(this.manager.getCurrentPlaylist().songs));
        this.manager.getCurrentPlaylist().addObserver(this::update);
    }

    /**
     *
     * @param o
     * @param arg
     */
    @Override
    public void update(Observable o, Object arg) {
        Playlist playlist = (Playlist) arg;
        root.setItems(FXCollections.observableList(playlist.songs));
    }

    /**
     *
     * @param event
     */
    public void handleDragOver(DragEvent event){
        Dragboard db = event.getDragboard();
        if (db.hasFiles()) {
            event.acceptTransferModes(TransferMode.COPY);
        } else {
            event.consume();
        }
    }

    /**
     *
     * @param event
     */
    public void handleDragDropped(DragEvent event) {
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasFiles()) {
            success = true;
            String filePath = null;
            for (File file : db.getFiles()) {
                manager.addFile(file);
            }
        }
        event.setDropCompleted(success);
        event.consume();
    }
}
