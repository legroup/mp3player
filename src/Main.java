import main.views.base.Base;

/**
 * @author Max Leonov, Jerrit Muscheites, Amatus Muscheites, Marvin Osswald
 */
public class Main extends Base {
    public static void main(String[] args) {
        launch(args);
    }
}
